#!/bin/bash

vos="atlas cms juno"
log_dir="/home/dbfrontier/logs"
script_dir="/home/dbfrontier/scripts/slsfrontier"
conf_dir="/home/dbfrontier/conf/slsfrontier"
data_dir="/home/dbfrontier/etc/slsfrontier"

exec >> ${log_dir}/sls_frontier.log 2>&1

echo

# only allow one to run at once; timeouts can make this take longer than
#  5 minutes
LOCKFILE=/dev/shm/slsfrontier.lock
echo $$ >$LOCKFILE.$$
trap "rm -f $LOCKFILE.$$" 0
if ! ln $LOCKFILE.$$ $LOCKFILE 2>/dev/null; then
    OTHERPID="`cat $LOCKFILE 2>/dev/null`"
    if [ -z "$OTHERPID" ] || ! kill -0 "$OTHERPID" 2>/dev/null; then
  rm -f $LOCKFILE
  if ! ln $LOCKFILE.$$ $LOCKFILE 2>/dev/null; then
      echo "Attempted to break lock at $LOCKFILE from $OTHERPID at `date` but failed, giving up"
      exit
  fi
    else
  echo "Attempted to start at `date` but process $OTHERPID still running"
  exit
    fi
    echo "Broke lock $LOCKFILE from $OTHERPID because process was not running"
fi
rm -f $LOCKFILE.$$
trap "rm -f $LOCKFILE" 0

# add timestamp
echo "Starting at `date`"

# invoke SLS Frontier monitors
for vo in $vos; do 
    $script_dir/sls_frontier.py $vo
done

# add upload timestamp
echo "Uploading at `date`"

#reporting to xsls was replaced by repoting to monit-metrics.cern.ch (which is done inside of the python script)
# Report to the xsls.cern.ch kibana monitoring
#for vo in $vos; do
#    cut -d\| -f1,4 $conf_dir/${vo}_frontiers.txt|tr '|' ' '| while read service id; do
#	echo uploading $id
#	curl -m 20 -sF file=@$data_dir/sls_${vo}_frontier_$service.xml xsls.cern.ch
#    done
#done

echo "Finished at `date`"
