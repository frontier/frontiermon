#! /usr/bin/python3
import os
import sys
import csv
# import subprocess
import socket
import urllib.request
import urllib.error
import http.client
from datetime import datetime, timezone
from bs4 import BeautifulSoup
import requests
import json
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import time
# import pprint


def monit(serviceid, servicestatus, info, desc, emails):
  report = {}
  report['producer'] = 'frontier'
  report['type'] = 'availability'
  report['serviceid'] = serviceid.lower()
  report['service_status'] = servicestatus
  report['availabilityinfo'] = info
  report['availabilitydesc'] = desc
  report['contact'] = emails
  report['webpage'] = 'http://frontier.cern.ch'
  return report


def test_frontier_shared(frontierurl, proxyurl):
  req = urllib.request.Request(frontierurl)
  req.add_header('X-frontier-id', 'SLS_probe 1.4 frontier.cern.ch')
  # this is how squid SAM tests handle the proxy
  handlers = []
  if proxyurl != '':
    handlers.append(urllib.request.ProxyHandler({'http': 'http://' + proxyurl + '/'}))
  else:
    # proxyurl is empty for launchpads
    req.add_header("pragma", "no-cache")

  try:
    opener = urllib.request.build_opener(*handlers)
    result = opener.open(req, timeout=20)
    #rmessage = BeautifulSoup(result.read(), "html.parser")
    rmessage = BeautifulSoup(result.read(), "xml")
    if 'Y2BgYLa0t' in str(rmessage.find_all("data")[0]):
      return [1, 'HTTP get status: ' + str(result.getcode())]
    else:
      try:
        return [0, 'quality error: ' + rmessage.find_all("quality")[0].attrs['message']]
      except:
        print(rmessage)
        return [0, 'error: expected response not present']
  except urllib.error.HTTPError as e1:  # HTTPError needs to go first as it is subset of URLError (which on the other hand does not have error code)
    return [0, 'HTTP get status: ' + str(e1.code)]
  except urllib.error.URLError as e2:
    return [0, 'HTTP get status: ' + str(e2.reason)]
  except http.client.BadStatusLine:
    return [0, 'httplib.BadStatusLine error']


print("SLS script started: " + str(datetime.now(tz=timezone.utc)) + ' UTC')

VO = sys.argv[1]
if os.path.exists('/home/dbfrontier/'):
  BASEDIR = "/home/dbfrontier/scripts/slsfrontier"
  CONFDIR = "/home/dbfrontier/conf/slsfrontier"
  OUTDIR = "/home/dbfrontier/etc/slsfrontier"
  frontiers_data_file = CONFDIR + "/" + VO + "_frontiers.txt"
else:
  BASEDIR = os.getcwd()
  CONFDIR = os.getcwd() + "/conf"
  OUTDIR = os.getcwd() + "/out/test"
  frontiers_data_file = CONFDIR + "/" + VO + "_frontiers.txt"

status_file = OUTDIR + "/" + VO + "_status.txt"

frontier_site = {}
site_name = ''
hn = []  # hosts
urls = []
idname = ''
frontierString = '/Frontier?type=frontier_request:1:DEFAULT&encoding=BLOBzip&p1=eNorTs1JTS5RsLS0VEgrys9VSClNzAEASFYG5g=='
# no try-except here - if the config file is not where it should be (or if there are some problems with its content), I don't want the script to silently fail - it should crash
with open(frontiers_data_file, 'r') as datafile:
  datareader = csv.reader(datafile, delimiter='|')
  for line in datareader:
    site_name = line[0]
    frontier_site[site_name] = {}
    idname = line[3]
    email = 'dwd@cern.ch,Michal.Svatos@cern.ch,'
    email += line[4]
    frontier_site[site_name]['id'] = idname
    frontier_site[site_name]['email'] = email
    frontier_site[site_name]['emailxml'] = line[4]
    frontier_site[site_name]['proxy_urls'] = []
    frontier_site[site_name]['frontier_urls'] = []
    frontier_site[site_name]['testname'] = []
    frontier_site[site_name]['pubstatus'] = ''
    frontier_site[site_name]['laststatus'] = ''
    hn = line[1].split(',')
    urls = line[2].split(',')
    for u in urls:
      for hosts in hn:
        if u[:7] == 'http://':
          frontier_site[site_name]['proxy_urls'].append(hosts)
          frontier_site[site_name]['frontier_urls'].append(u + frontierString)
          frontier_site[site_name]['testname'].append(hosts + '/' + u[7:])
        else:
          frontier_site[site_name]['proxy_urls'].append('')
          frontier_site[site_name]['frontier_urls'].append('http://' + hosts + '/' + u + frontierString)
          frontier_site[site_name]['testname'].append('/' + hosts + '/' + u)
# pprint.pprint(frontier_site)

site_name2 = ''
pubstatus = ''
laststatus = ''
try:
  with open(status_file, 'r') as statusfile:
    statusreader = csv.reader(statusfile, delimiter='|')
    for line in statusreader:
      site_name2 = line[0]
      pubstatus = line[1]
      laststatus = line[2]
      frontier_site[site_name2]['pubstatus'] = pubstatus
      frontier_site[site_name2]['laststatus'] = laststatus
except:
  print("Cannot read status file\n")

# keys of badinst
# cmsmeyproxy1.cern.ch:3128/cmsfrontier.cern.ch:8000/PromptProd
# /cmsfrontier1.cern.ch:8000/FrontierArc
with open(status_file, 'w') as statusfile:
  for site in sorted(frontier_site.keys()):
    timestamp = str(datetime.now(tz=timezone.utc).replace(microsecond=0).isoformat())
    avail = 0
    availability = 0
    availabilityinfo = ''
    availabilityinfostatus = ''
    badinst = {}
    for i in range(0, len(frontier_site[site]['frontier_urls'])):
      testresults = []
      try:
        testresults = test_frontier_shared(frontier_site[site]['frontier_urls'][i], frontier_site[site]['proxy_urls'][i])
      except socket.timeout:
        testresults = [0, 'HTTP get status: socket.timeout']
      avail += 100 * testresults[0]
      if testresults[0] == 0:  # for the degraded
        badinst[frontier_site[site]['testname'][i]] = testresults[1]
    availability = avail / len(frontier_site[site]['frontier_urls'])
    # availability
    status = ''
    if availability == 100:
      status = 'available'
    elif availability == 0:
      status = 'unavailable'
    else:
      status = 'degraded'
    # availabilityinfo
    monitdesc = 'Hosts monitored: '
    monitinfo = ''
    availabilityinfo += 'Hosts monitored:<br></br>'
    for i in range(0, len(frontier_site[site]['frontier_urls'])):
      availabilityinfo += frontier_site[site]['testname'][i]
      monitdesc += frontier_site[site]['testname'][i]
      monitdesc += '; '
      availabilityinfo += '<br></br>'
    if len(badinst) == 0:
      availabilityinfostatus += 'All hosts OK'
      monitinfo += 'All hosts OK'
    else:
      availabilityinfostatus += 'Service broken on host(s): '
      monitinfo += availabilityinfostatus
      monitinfo += '; '
      availabilityinfostatus += '<br></br>'
      for badfrontier in sorted(badinst.keys()):
        monitinfo += badfrontier + ' (' + badinst[badfrontier] + '); '
        availabilityinfostatus += badfrontier + ';' + badinst[badfrontier]
    availabilityinfo += availabilityinfostatus
    # sending to monit metrics
    if os.path.exists('/home/dbfrontier/'):
      monitreport = monit(frontier_site[site]['id'], status, monitinfo, monitdesc, frontier_site[site]['emailxml'])
      os.environ['http_proxy'] = ''
      os.environ['HTTP_PROXY'] = ''  # otherwise the script will try to send the info via http proxy set during the testing
      response = requests.post('http://monit-metrics:10012/', data=json.dumps(monitreport), headers={"Content-Type": "application/json; charset=UTF-8"})
      if response.status_code != 200:
        print(response.text)
    ##########################################
    # how to avoid publication of fluctuation - I have:
    # -status - current state
    # -laststatus - previous state
    # -pubstatus - the most recent status announced by email before laststatus happened, i.e. it represents the trend before the laststatus
    # degradation is not fluctuation if the current and previous are the same but the trend reported by pubstatus is different
    ##########################
    # statuses combinations and appropriate script actions:
    # pubstatus-laststatus-status
    # ---------------------------
    # available-available-available
    # -nothing happens
    # -written into the status file: pubstatus=available,laststatus=available
    # available-available-degraded
    # -starting, i.e. status is degraded but everything else is available - only the printed message in else statement
    # -written into the status file: pubstatus=available,laststatus=degraded
    # available-degraded-available - degradation fluctuation
    # -nothing happens
    # -written into the status file: pubstatus=available,laststatus=available
    # available-degraded-degraded - degradation continues
    # -email is triggered
    # -written into the status file: pubstatus=degraded,laststatus=degraded
    # degraded-degraded-degraded  degradation continues
    # -nothing happens
    # -written into the status file: pubstatus=degraded,laststatus=degraded
    # degraded-degraded-available - degradation is fixed
    # -only the printed message in else statement
    # -written into the status file: pubstatus=degraded,laststatus=available
    # degraded-available-degraded - degradation returns
    # -nothing happens
    # -written into the status file: pubstatus=degraded,laststatus=degraded
    # degraded-available-available - degradation fixed
    # -email is triggered
    # -written into the status file: pubstatus=available,laststatus=available
    ##########################
    # long degradation VS fluctuation
    # DDDAA - email is sent during DD phase, i.e. pubstatus is D
    # AADAA - email is not sent, i.e. pubstatus is A
    emailtext = ''
    pubstatus = frontier_site[site]['pubstatus']
    laststatus = frontier_site[site]['laststatus']
    if pubstatus != status:  # i.e. do nothing for stable situation (available-available-available, degraded-degraded-degraded) or  fluctuation (available-degraded-available, degraded-available-degraded)
      if pubstatus == "":  # in case of empty status - this should really happen only when the script runs for a first time - setting published status to current status
        pubstatus = status
        print("Published status is empty, using current status instead\n")
      elif laststatus != status:  # starting degradation or just fluctuation (available-available-degraded, degraded-degraded-available)
        print("Status changed from " + pubstatus + " to " + status + " but delaying notice since last check it was " + laststatus + "\n")
      else:  # i.e. getting to (available-degraded-degraded) or from (degraded-available-available) degradation
        if email != '':
          print("Sending notice to " + email + " of change from " + pubstatus + " to " + status + "\n")
          emailtext += str(availabilityinfostatus) + "\n"
          emailtext += "\n"
          emailtext += "History available at\n"
          emailtext += 'https://monit-grafana.cern.ch/d/plTtqczZz/sls-details?orgId=17&var-services=' + frontier_site[site]['id'] + '\n'
          # try:
          #   process = subprocess.Popen(['mail', '-s', site+' status changed from '+pubstatus+' to '+status, email],stdin=subprocess.PIPE)
          #   process.communicate(emailtext.encode('utf-8'))
          # except Exception as error:
          #   print('Email sending was affected by the following error: '+str(error))
          sender = 'dbfrontier@mail.cern.ch'  # From
          receivers = email  # To
          # copies = ''  # CC
          msg = MIMEMultipart('alternative')  # Using “alternative” indicates that the email message contains multiple representations of the same content, but in different formats. For example, you might have the same email message in both plain text and HTML.
          msg['Subject'] = site + ' status changed from ' + pubstatus + ' to ' + status
          msg['To'] = receivers
          msg.attach(MIMEText(emailtext, 'plain'))
          try:
            smtpObj = smtplib.SMTP('localhost')
            # smtpObj.set_debuglevel(True)
            # smtpObj.sendmail(sender, [receivers] + [copies], msg.as_string())
            smtpObj.sendmail(sender, [receivers], msg.as_string())
            smtpObj.quit()
            print("\nSuccessfully sent email to: " + msg['To'])
            # print(" CC'ed to: "+msg['CC'])
            time.sleep(1)
          except:
            print("\nFailed to send email to: " + msg['To'])

        pubstatus = status  # email is sent, so setting the pubstatus to current status
    if status == 'available':
      print(frontier_site[site]['id'] + ' ' + status)
    else:
      print(frontier_site[site]['id'] + ' ' + availabilityinfo)
    writer = csv.writer(statusfile, delimiter='|')
    writer.writerow([site, pubstatus, status])
print("SLS script ended: " + str(datetime.now(tz=timezone.utc)) + ' UTC')
