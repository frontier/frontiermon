#!/usr/bin/perl
#
# script to generate SLS dynamic information for ATLAS and CMS FroNTier

# author: Florentin Bujor
# contact: florentin.bujor@cern.ch
# $Id: sls_atlas-cms_frontier.pl 58 2011-10-17 08:26:28Z florentin.bujor $
#
# modified by Ran Du (ran.du@cern.ch) 09-Aug-2012
# modification list:
# 1. remove hardcoded cms/atlas
# 2. add T0 Test for ATLAS
# 3. modify config .txt files for cms and atlas: atlas_frontiers.txt, cms_frontiers.txt

use LWP::Simple;
use LWP::UserAgent;
use HTTP::Headers;
use Data::Dumper;
use integer;

# change $DEBUG to 1 if debugging, else $DEBUG is 0
# $DEBUG_AVAIL is used to check $availability
my $DEBUG = 0;
my $DEBUG_AVAIL = 0;

# global variables
my %frontier_site = ();
my $VO = $ARGV[0];
my $BASEDIR = "/home/dbfrontier/scripts/slsfrontier";
my $xml_template = "$BASEDIR/sls_frontier_template.xml";
#my $xml_template = "$BASEDIR/test_sls_frontier_template.xml";
my $CONFDIR = "/home/dbfrontier/conf/slsfrontier";
my $OUTDIR = "/home/dbfrontier/etc/slsfrontier";

# print only for test
print "xml_template is $xml_template\n" if $DEBUG_AVAIL;
if($VO){
    #$frontiers_data_file="$OUTDIR/test_$VO"."_frontiers.txt"
    $frontiers_data_file="$CONFDIR/$VO"."_frontiers.txt";
    $status_file="$OUTDIR/$VO"."_status.txt";
}
else{
    print "Usage: sls_atlas-cms-frontier.pl voname\n";
    exit;
}

print "frontiers_data_file is : $frontiers_data_file\n\n" if $DEBUG;

# Read params from config files
open(FrH, $frontiers_data_file) || die("Could not open frontiers data file!");

while( <FrH> ){
    chomp($_);
    ($site_name,$proxies,$frontier_urls,$id,$email)=split( /\|/ );

    # print only for test
    print "site_name is: $site_name \n" .
         "proxies is $proxies \n" .
         "frontier_urls is $frontier_urls \n" .
         "id is $id \n" .
         "email is $email \n" .
         "====================================================\n" if $DEBUG;
    @hn = split(/,/, $proxies);
    # print only for test
    print "hn is : @hn\n" if $DEBUG;
    @temp_array = @hn;

    # print only for test
    print "hn is : @hn\n" if $DEBUG;

    @urls = split(/,/, $frontier_urls);
    # print only for test
    print "urls is: @urls\n" if $DEBUG;

    ###### Modification: change the sequence of cycling: servlet first, host second ######
    foreach my $url (@urls) {
        foreach my $host (@hn) {
       my $hosturl;
            if($url =~ "http://"){
      # Full URL given, so the "host" is actually a proxy
      #  and the url is a full URL of the launchpad
      push @{ $frontier_site{ $site_name }{ 'proxy_urls'} }, $host;
      $hosturl = $url
       }else{
      # Only servlet given, so this is for a launchpad.
      # Set an empty proxy and expand the host url.
      push @{ $frontier_site{ $site_name }{ 'proxy_urls'} }, "";
      $hosturl = "http://" . $host . "/" . $url;
            }
       push @{ $frontier_site{ $site_name }{ 'frontier_urls' } }, $hosturl . '/Frontier?type=frontier_request:1:DEFAULT&encoding=BLOBzip&p1=eNorTs1JTS5RsLS0VEgrys9VSClNzAEASFYG5g==';
        }
    }

    $frontier_site{ $site_name }{ 'id' } = $id;
    $frontier_site{ $site_name }{ 'email' } = $email;
}# end of while <FrH>
close(FrH);
# print only for test
print Dumper(%frontier_site) if $DEBUG;

##############

# read the old status for each site from the status file
open(SF, $status_file);
while( <SF> ){
    chomp($_);
    ($site_name,$pubstatus,$laststatus)=split( /\|/ );
    $frontier_site{ $site_name }{ 'pubstatus' } = $pubstatus;
    $frontier_site{ $site_name }{ 'laststatus' } = $laststatus;
}# end of while <SF>
close(SF);

# and write it with the new values
open(SF, ">$status_file") or die "Cannot write status file\n";

# test Frontier servlets one by one, and record the result to the specified xml file
for my $site_name ( keys %frontier_site ) {

   @frontier_urls = @{ $frontier_site{ $site_name }->{ 'frontier_urls' } };
   @proxy_urls = @{ $frontier_site{ $site_name }->{ 'proxy_urls' } };
   $id = $frontier_site{ $site_name }->{ 'id' };
   $email = $frontier_site{ $site_name }->{ 'email' };
   $pubstatus = $frontier_site{ $site_name }->{ 'pubstatus' };
   $laststatus = $frontier_site{ $site_name }->{ 'laststatus' };

   # $len only for test
   $len = @frontier_urls;

   # print only for test
   print "frontier_urls is: @frontier_urls \n".
         "proxy_urls is : @proxy_urls\n".
         "id is :  $id \n".
         "email is :  $email \n".
         "pubstatus is :  $pubstatus \n".
         "laststatus is :  $laststatus \n".
         "len is:  $len \n".
         "=============================================================\n" if $DEBUG;
   $xml_path = "$OUTDIR/sls_". $VO . "_frontier_" . $site_name . ".xml";
   #$xml_path = $OUTDIR . "/test_sls_". $VO . "_frontier_" . $site_name . ".xml";

   ###### sitetimeout -> 60 seconds ######
   $sitetimeout = 60;
   my @badinst = ();
   my @hosts = ();
   my $availability = 0;
   my $testname = "";
   eval {
      #SIGALRM is delivered to this process after sitetimeout in seconds has elapsed.
      local $SIG{ALRM} = sub { die "alarm timeout\n" };
      alarm $sitetimeout;

      my $index = 0;
      while ($index < $len){
    my $frontier_url = $frontier_urls[$index];
    my $proxy_url = $proxy_urls[$index];
    # for launchpad, testname is launchpadname:port/servlet
    # for proxy, testname is proxyname:port/launchpadname:port/servlet
    $testname = $frontier_url;
    $testname =~ s#.*//(.*)/Frontier.*#$1#;
    if ($proxy_url == "") {
       $testname = $proxy_url . '/' . $testname;
    }
    print "testing $testname\n" if $DEBUG;

    my $isok, $testdata;
    ($isok, $testdata) = &test_frontier_shared($frontier_url, $proxy_url);

    # get $availability
    $availability += $isok * 100;
    # print only for test
    print "availability is $availability \n" if $DEBUG_AVAIL;
    # get @hosts
    my %hhosts = map { $_ => 1 } @hosts;
    $hhosts{$testname} = 1 unless exists $hhosts{$testname};
    @hosts = keys %hhosts;

    # get @badinst
    if (!$isok) {
       # modified by Ran Du, add $servlet to the error message, 17-Sep-12
       push @badinst, $testname . '; ' . $testdata;
    } # endif
    $index += 1;

      }# end of while index
      $availability /= $index;
      # print only for test
      print "**index is : $index \n".
       "**availability is : $availability \n" if $DEBUG_AVAIL;
      alarm 0;
   }; #end eval
   if($@) {
      # an exception occurred in the previous eval statement
      die unless $@ eq "alarm timeout\n";  # propagate unexpected errors
      # timed out
      push @badinst, 'site test timed out during '. $testname;
   }

##############

   $timestamp = &timestamp;

   ###### Modification by Ran Du(ran.du@cern.ch): make the alias names unique ######
   #my @alias = sort( &uniq( map{($_)= split(/\:/, $_); "<host>$_</host>"} @hosts ) );
   $lemonhost = join("<br></br>", "Hosts monitored:", sort(@hosts));

   $data = "All hosts OK";
   if (@badinst) {
      $data = "Service broken on host(s):";
      foreach my $i (@badinst) {
         $data .= "<br></br>$i";
      }
   }
   if (!$site_name) {
      $data= " No Hosts in Production Mode";
   }
   # put the data into the log as well
   #$ts = $timestamp;
   #$ts =~ s/T/ /g;
   #$ts =~ s/\+00:00/ GMT/g;
   $logdata = $data;
   $logdata =~ s/<br><\/br>/\n/g;
   print $id." ".$logdata."\n";
   $id_name = $id;
   $avail = $availability;
   if ($availability == 100) {$status = "available";}
   elsif ($availability == 0) {$status = "unavailable";}
   else {$status = "degraded";}
   # print only for test
   print "++++ availability is $availability\n" if $DEBUG_AVAIL;
   local $/;
   open(T, "$xml_template") or die "Cannot find XML template\n";
   $xml = <T>;
   # print only for test
   print "xml is $xml\n" if $DEBUG_AVAIL;
   close(T);
   # print only for test
   print "++++ availability is $availability\n" if $DEBUG_AVAIL;
   $xml =~ s{ %% ( .*? ) %% } { $$1 }gsex;
   # print only for test
   print "++++ availability is $availability\n" if $DEBUG_AVAIL;

   open(F, ">$xml_path") or die "Cannot write XML file\n";
   print F $xml;
   # print only for test
   print "xml is $xml\n" if $DEBUG_AVAIL;
   close(F);

   if ($pubstatus ne $status) {
      if ($pubstatus eq "") {
         # work around problem where $pubstatus sometimes gets set to empty
    print "Published status is empty, using last status instead\n";
         $pubstatus = $laststatus;
    $laststatus = $status;
      }
      if ($laststatus eq $status) {
    if ($email ne "") {
       print "Sending notice to $email of change from $pubstatus to $status\n";
       open(MF,"|mail -s \"$site_name status changed from $pubstatus to $status\" $email dwd\@fnal.gov");
       print MF $data."\n";
       print MF "\n";
       print MF "History available at\n";
       print MF "https://monit-grafana.cern.ch/d/000000855/overview-service-availability?orgId=1&var-isfe=All&var-regexp=%5e$id%24\n";
       close MF;
    }
    $pubstatus = $status;
      }
      else {
    print "Status changed from $pubstatus to $status, but delaying notice since last check it was $laststatus\n";
      }
   }

   print SF $site_name."|".$pubstatus."|".$status."\n";

##############
}
close(SF);

##############

##======= sub routines ========
# description: combine frontier lauchpad & T0 voboxes tests together
# params: frontier_url, proxy_url
# sample of $frontier_url: http://cmsfrontier.cern.ch:8000/FrontierProd # both for Frontier Launchpad & T0 machines
# Note that $proxy_url isn't a full URL; it is missing the http://
# $proxy_url is empty for launchpads
# sample of $proxy_url: cmst0frontier1.cern.ch:3128 <==== Frontier T0 machines
# return value: (0|1, test_data)
sub test_frontier_shared{

    my $frontier_url = shift;
    my $proxy_url = shift;

    my $ua = LWP::UserAgent->new;
    if ($proxy_url != ""){$ua->proxy('http', "http://$proxy_url/");}
    $ua->default_header('X-frontier-id' =>
         'SLS_probe 1.4 frontier.cern.ch');
    # force a cache flush when connecting directly to launchpads
    if ($proxy_url == ""){$ua->default_header('Pragma' => 'no-cache');}

    ###### modification: modify the timeout to 25 seconds ######
    $ua->timeout(25);
    my $t0 = &timestamp;
    my $response = $ua->get($frontier_url);
    my $t0 = &timestamp;
    $testdata = "HTTP get status: ". $response->status_line( );
    unless ($response->is_success) {
   return (0, $testdata);
    }
    if ($response->decoded_content =~ /<data>.*Y2BgYLa0t/s) {
        return (1, $testdata);
    } else {
   # Ideally we would look for a <global_error> xml tag and
   #  include that message in the return value here if present
   my $url = $frontier_url;
   if ($proxy_url != ""){$url = $proxy_url;}
   print "#Decoded response content for $url:#\n", $response->decoded_content,"\n\n";
   return (0, "expected response not present");
    }

}

# description: get datetime of 'NOW'
# params: no params
# return values: e.g., 2012-08-14T14:41:03+00:00
sub timestamp {

    my @time = gmtime(time);
    my $timestamp = sprintf("%s-%02d-%02dT%02d:%02d:%02d+00:00",
             (1900+$time[5], 1+$time[4], $time[3],
              $time[2], $time[1], $time[0]));
    return $timestamp;
}

# description: get the alias name of one machine
# params: hostname, e.g., cmsfrontier1.cern.ch
# return values: alias name, e.g., vocms213
# not used anymore
sub aliases {
    my $nd = @_[0];
    my $command = 'host ' . $nd . '|sed -n \'$s/\..*//p\'';
    my $hostname = `$command` ;
    return $hostname;
}

###### Modification: subroutine uniq is used to make the alias names unique ######
# description: return uniq items of an array
# params: an array
# return values: an array without repetition
sub uniq{
    return keys %{{map{$_ => 1} @_}};
}
