#!/bin/bash

SHELL=/bin/sh

echo "$(date '+%Y-%m-%d %H:%M:%S') - [INFO] Starting mastercms script"
/home/dbfrontier/scripts/monitcms/monitsquids.sh
/home/dbfrontier/scripts/status.sh
/home/dbfrontier/scripts/frontiermon.sh
/home/dbfrontier/scripts/wlcgsquidmon.sh

