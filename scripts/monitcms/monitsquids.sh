#!/bin/bash

cd /home/dbfrontier/etc/monitcms/

menu_file=menucms.html
url=http://wlcg-squid-monitor.cern.ch/snmpstats/
wget -q -O $menu_file $url$menu_file
trap "rm -f $menu_file" 0

# $1 - status
# $2 - squid
# $3 - html body
# $4 - squid link
function status {
    status_file="$2.status"
    if [ ! -f "$status_file" ]; then
        echo "LAST=OK" >> $status_file
        echo "PENULTIMATE=OK" >> $status_file
        echo "EMAIL=OK" >> $status_file
        echo "FIRST_FAIL=None" >> $status_file
    fi
    LAST=`grep -oP '(?<=LAST=).*' $status_file`
    PENULTIMATE=`grep -oP '(?<=PENULTIMATE=).*' $status_file`
    EMAIL=`grep -oP '(?<=EMAIL=).*' $status_file`
    FIRST_FAIL=`grep -oP '(?<=FIRST_FAIL=).*' $status_file`
    if [ "$1" == "$LAST" ] ; then
       if [ "$LAST" != "$PENULTIMATE" ] ; then
          sed -i "s/PENULTIMATE=\(.*\)/PENULTIMATE=$LAST/" $status_file
          if [ "$1" == "$EMAIL" ] && [ "$1" == "OK" ] ; then
              sed -i "s/FIRST_FAIL=\(.*\)/FIRST_FAIL=NONE/" $status_file
          fi
       else
          if [ "$1" != "$EMAIL" ] ; then
              if [ "$1" == "OK" ] ; then
                  text="$2 squid has returned"
                  level="OK"
                  echo -e "$text\n"`date '+%Y-%m-%d %H:%M:%S'` | mail -s "$2 squid has returned" cms-frontier-alarm@cern.ch
                  sed -i "s/FIRST_FAIL=\(.*\)/FIRST_FAIL=NONE/" $status_file
              else
                  text="$2 squid is not responding"
                  level="ERROR"
                  echo -e "$text\n$FIRST_FAIL" | mail -s "No $2 squid" cms-frontier-alarm@cern.ch
              fi
              sed -i "s/EMAIL=\(.*\)/EMAIL=$1/" $status_file
              echo "$(date '+%Y-%m-%d %H:%M:%S') - [$level] $text"
          fi
       fi
    else
       sed -i "s/LAST=\(.*\)/LAST=$1/" $status_file
       if [ "$LAST" != "$PENULTIMATE" ] ; then
          sed -i "s/PENULTIMATE=\(.*\)/PENULTIMATE=$LAST/" $status_file
       else
          if [ "$1" != "$EMAIL" ] && [ "$1" == "FAILED" ] ; then
              FIRST_FAIL=`date "+%Y-%m-%d %H:%M:%S"`
              sed -i "s/FIRST_FAIL=\(.*\)/FIRST_FAIL=$FIRST_FAIL/" $status_file
          fi
       fi
    fi

    if [ "$1" == "FAILED" ] || [ "$LAST" == "FAILED" ] || [ "$PENULTIMATE" == "FAILED" ] ; then
        if [ "$1" == "FAILED" ] && [ "$LAST" == "FAILED" ] && [ "$PENULTIMATE" == "FAILED" ] ; then
            badge="danger"
            badge_text="Broken"
        else
            badge="warning"
            badge_text="Warning"
        fi
        badge_title="The last three responses are $1, $LAST, $PENULTIMATE"
        html_body=$3
        squid=${2##*_}
        site=${2%_*}
        if [[ $squid =~ ^[0-9]+$ ]] ; then
            squid="${site##*_}_$squid"
            site=${site%_*}
        fi
        read -r -d '' html_body << EOM
$html_body
          <tr>
            <td scope="row"></td>
            <td>$site</td>
            <td>
              <a target="_blank" href="$4">$squid</a>
            </td>
            <td>$FIRST_FAIL</td>
            <td class="text-center" title="$badge_title"><span class="badge badge-$badge">$badge_text</span></td>
          </tr>
EOM
    fi

    echo "$html_body"
}

if [ -f "$menu_file" ] ; then
    if [ -s "$menu_file" ] ; then
        proxy_file="proxy-hit.html"
        proxy_hits=`grep proxy-hit.html $menu_file | sed 's/.*href="//g' | sed 's/" target.*//g'`
        html_body=""
        for proxy_hit in $proxy_hits; do
            squid=${proxy_hit%/*}
            squid=${squid#*/}
            if [[ $squid != T3_* ]] ; then
                proxy_url=$url$proxy_hit
                wget -q -O $proxy_file $proxy_url
                if [ -f "$proxy_file" ] ; then
                    if [ -s "$proxy_file" ] ; then
                        if ! grep -q "Description.*&" $proxy_file; then
                            index_file="index.html"
                            if grep -q "time" $proxy_file ; then
                                html_body=$(status "OK" $squid "$html_body" ${proxy_url/$proxy_file/$index_file})
                            else
                                html_body=$(status "FAILED" $squid "$html_body" ${proxy_url/$proxy_file/$index_file})
                            fi
                        fi
                    else
                        echo "$proxy_file file is empty for $squid"
                    fi
                    rm -f $proxy_file
                else
                    echo "$proxy_file file is not retrieved from $url$proxy_hit"
                fi
            fi
        done
        if [ -z "$html_body" ] ; then
            html_div='\n      <div class="text-center">All squids are responding <span class="badge badge-success">OK</span></div>'
        fi
        read -r -d '' html << EOM
<html>
  <head>
    <title>Status of squids</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
      tbody>tr:hover {background: #cecece !important;}
      table {
        counter-reset: row-num;
      }
      table tr td:first-child::before {
        counter-increment: row-num;
        content: counter(row-num)".";
      }
    </style>
  </head>

  <body>
    <div class="container">
      <h1>List of currently not responding squids</h1>
      <p>Last update was on `date "+%Y-%m-%d %H:%M:%S"`</p>
      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Site</th>
            <th scope="col">Squid</th>
            <th scope="col">Inactive since</th>
            <th class="text-center" scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          $html_body
        </tbody>
      </table>$html_div
    </div>
  </body>
</html>
EOM
        output="/home/dbfrontier/www/squidstats/squid_status.html"
        output_new="${output}.new"
        output_old="${output}.old"
        echo -e "$html" >> $output_new
        if [ -f $output_old ] ; then
            rm $output_old
        fi
        if [ -f $output ] ; then
            ln $output $output_old
        fi
        mv $output_new $output
    else
        echo "$menu_file file is empty"
    fi
else
    echo "$menu_file file is not retrieved from $url$menu_file"
fi

