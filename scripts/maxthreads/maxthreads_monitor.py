#! /usr/bin/python3
"""
created by Weizhen.Wang@cern.ch, v1, Dec 17, 2010
v3, Dec 01, 2011, by Weizhen.Wang@cern.ch, add configure file for different site
"""

import os
import sys
from socket import gethostbyname, gethostbyname_ex
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import time


## some global settings
pref1='chkthread_'
pref2='maxthreads.'
configpre1='/maxthreads_monitor.config.'

project = sys.argv[1]
siteconfig = {}
histday=2

# these are initialized later per project
srcdir=""
nodes=""
imgdir=""
pgsdir=""
almdir=""
mailaddr=""
mailhead_alm=""
mailhead_red=""
mailmsg_alm=""
mailmsg_red=""
if os.path.exists('/home/dbfrontier/'):
  confdir="/home/dbfrontier/conf/"
  os.chdir("/home/dbfrontier/etc/maxthreads/")
else:
  confdir=os.getcwd()
  os.chdir(os.getcwd())

# end of globals


def getconfig():

  global siteconfig

  fobj = open(confdir+configpre1+project,'r')
  for line in fobj:
       oneline = line.strip()
       if not oneline:
           continue 
       if oneline[0] == '#':
           continue

       a = oneline
       if a[0:5] == '[site':
           sitename = a.split()[1].replace(']','').strip()
           siteconfig[sitename] = {}
       else:
           elem = a.split('=')
           if elem[0] == 'servers':
              siteconfig[sitename]['servers'] = elem[1].split(',')
           else:
              siteconfig[sitename][elem[0]] = elem[1]

def siteinit(sitename):

  global srcdir
  srcdir = siteconfig[sitename]['srcdir']
  global nodes
  nodes = siteconfig[sitename]['servers']

# srcfile prefix: /home/dbfrontier/data/awstats/cerncms/chkthread_cmsfrontier1/maxthread.cmsfrontier1.2010-12-29
#pref1='chkthread_'
#pref2='maxthreads.'

  global imgdir,pgsdir,almdir
  global mailaddr

  imgdir='./maxthreads_monitor.'+project+'.plots/'

  pgsdir='./maxthreads_monitor.'+project+'.pages/'
#alarmdir
  almdir='./maxthreads_monitor.'+project+'.mails/'

#mailsddress
  mailaddr=siteconfig[sitename]['mailaddr']

  global mailhead_alm,mailhead_red,mailmsg_alm,mailmsg_red
  mailhead_alm=\
  """Subject: Threads of TO_REPLACE_INSTANCE on TO_REPLACE_NODE exceed threshold
  """ + 'To: %s\n\n'%mailaddr 

  mailhead_red=\
  """Subject: Threads of TO_REPLACE_INSTANCE on TO_REPLACE_NODE reduced
  """ + 'To: %s\n\n'%mailaddr 

  mailmsg_alm=\
  """
  The number of threads of instance TO_REPLACE_INSTANCE on TO_REPLACE_NODE is TO_REPLACE_NUMBER, 
  greater than TO_REPLACE_THRESHOLD (75% of the maxthreads setting).
  For more information, please check http://frontier.cern.ch/maxthreads/frontier_inst_maxthreads"""+'.'+project+'.html'

  mailmsg_red=\
  """
  The number of threads of instance TO_REPLACE_INSTANCE on TO_REPLACE_NODE is now TO_REPLACE_NUMBER, less than TO_REPLACE_THRESHOLD_LOW (25% of the maxthreads setting).
  For more information, please check http://frontier.cern.ch/maxthreads/frontier_inst_maxthreads"""+'.'+project+'.html'


def getInfo(datafile):
# return mxth
# mxth: a dict, with instance as key, 
#     mxth[instance] is a dict with ['tms','mth','avt'] as key
#       mxth[instance]['tms'] is a list, 1d, length N
#       mxth[instance]['mth'] is a list, 1d, length N
#       mxth[instance]['avt'] is a list, 2d, N*2
#       mxth[instance]['th_th'] is a list, 1d, length N
  mxth = {}
  try:
    fobj = open(datafile,'r')

    # 2011/01/15 20:49:39 FrontierProd maxthreads=1 averagetime=4.25455 msec avedbquerytime=2.92308 msec threadsthreshold=150
    for line in fobj:
       oneline = line.strip()
       if oneline[0] == '#':
           continue

       elems = oneline.split()
       ts = elems[0] + ' ' + elems[1]
       tm = time.mktime( time.strptime(ts,'%Y/%m/%d %H:%M:%S') )
       ist = elems[2]       
       mxthv_str = elems[3].replace('maxthreads=','')
       avtmv_str = elems[4].replace('averagetime=','') 

       try:
           mxthv = int(mxthv_str)
       except ValueError:
           mxthv = 0

       if avtmv_str != '':
           try:
               avtmv = float (avtmv_str)
           except ValueError:
               avtmv = 0
       else:
           avtmv = 0

       if len(elems) > 6:
          try:
              dbtmv = float(elems[6].replace('avedbquerytime=',''))
          except ValueError:
              dbtmv = 0
       else:
          dbtmv = 0

       if len(elems) > 8:
          try:
              th_threshold = float(elems[8].replace('threadsthreshold=',''))
          except ValueError:
              th_threshold = 0
       else:
          th_threshold = 100000

       if ist in mxth.keys():#mxth.has_key(ist):
          mxth[ist]['tms'].append(tm)
          mxth[ist]['mth'].append(mxthv)
          mxth[ist]['avt'].append([avtmv,dbtmv])
          mxth[ist]['th_th'].append(th_threshold)
       else:
          mxth[ist]={}
          mxth[ist]['tms']=[tm]
          mxth[ist]['mth']=[mxthv]
          mxth[ist]['avt']=[[avtmv,dbtmv]]
          mxth[ist]['th_th']=[th_threshold]

    fobj.close()

  except IOError:
    pass 

  except Exception as e:
    # workaround for bugs on sending side
    sys.stderr.write("ERROR: " + "There was an error processing file " + datafile + ":\n" + str(e) + '\n')

  return mxth 

def combine(mxthss):
#   allkeys = list(set(infos[1])|set(infos[2])|set(info[3]))
   mxth_c={}
   keyset=set([])

# mxthss: list of maxth 7-day history
# mxth_c: combine the 7-day history
# key is servlet

   for mxth1 in mxthss:
       keyset = keyset.union(set(mxth1.keys()))
   allkeys = list(keyset)
   for key in allkeys:
      mxth_c[key]={}
      mxth_c[key]['tms']=[]
      mxth_c[key]['mth']=[]
      mxth_c[key]['avt']=[]
      mxth_c[key]['th_th']=[]
      for mxth1 in mxthss:
         if key in mxth1.keys():#mxth1.has_key(key):
            mxth_c[key]['tms'].extend(mxth1[key]['tms'])
            mxth_c[key]['mth'].extend(mxth1[key]['mth'])
            mxth_c[key]['avt'].extend(mxth1[key]['avt'])
            mxth_c[key]['th_th'].extend(mxth1[key]['th_th'])
   return mxth_c 

def combine_nodes(mxth_cs):
#   allkeys = list(set(infos[1])|set(infos[2])|set(info[3]))
   mxth_c={}
   keyset=set([])
   for mxth1 in mxth_cs:
       keyset = keyset.union(set(mxth1.keys()))
   allkeys = list(keyset)
   for key in allkeys:
      tmin = 1e30
      tmax = 0

      mxth_c[key]={}
      mxth_c[key]['tms']=[]
      mxth_c[key]['mth']=[]
      mxth_c[key]['avt']=[]
      mxth_c[key]['th_th']=[]
      avt_count=[]
      
      for  mxth1 in mxth_cs:
         if key in mxth1.keys():#mxth1.has_key(key):
            if tmax < mxth1[key]['tms'][-1]:
               tmax = mxth1[key]['tms'][-1]
            if tmin > mxth1[key]['tms'][0]:
               tmin = mxth1[key]['tms'][0]
      deltat = 300
      tmin=round(tmin/deltat)*deltat
      tmax=round(tmax/deltat)*deltat
      my_tms = np.arange(tmin,tmax+deltat,deltat)

      mxth_c[key]['tms']=my_tms
      mxth_c[key]['mth']=np.zeros(np.shape(my_tms))
      mxth_c[key]['avt']=np.zeros((np.shape(my_tms)[0],2))
      avt_count=np.zeros(np.shape(mxth_c[key]['avt']))

      for  mxth1 in mxth_cs:
         if key in mxth1.keys():#mxth1.has_key(key):
#          print "%d\n"%len(mxth_c[key]['tms'])
            mthdata = np.interp(my_tms,mxth1[key]['tms'],mxth1[key]['mth'])
            avtdata1 = np.interp(my_tms,mxth1[key]['tms'],np.array(mxth1[key]['avt'])[:,0])
            avtdata2 = np.interp(my_tms,mxth1[key]['tms'],np.array(mxth1[key]['avt'])[:,1])
            avtdata = np.column_stack((avtdata1,avtdata2))

#          print tlength
#          print len(mxth1[key]['tms'])
#          print np.shape(mthdata)

            mxth_c[key]['mth']+=mthdata
            mxth_c[key]['avt']+=avtdata
            avt_count+=np.array(avtdata>0,dtype='float')

      avt_count[avt_count==0]=3
      mxth_c[key]['avt']=mxth_c[key]['avt']/avt_count
   return mxth_c 

def mycolormap(iindex):

  imin=0
  imax=24

  v_r = min(255, int(255/(imax-imin)*(imax-iindex)*2))
  v_g = min(255, int(255/(imax-imin)*(iindex-imin)*2))
  v_b = 0

  return '"#%02x%02x%02x"'%(v_r,v_g,v_b)  

def genchart(mxth,vflag,svnode,histday_plot):
#generate graph
#  mxth: a dict, with instance as key, 
#     mxth[instance] is a dict with ['tms','mth','avt'] as key
#     mxth[instance]['tms'] is a list, 1d, length N
#     mxth[instance]['mth'] is a list, 1d, length N
#     mxth[instance]['avt'] is a list, 2d, N*2
#  vflag: variable 'mth' or 'avt'
#  svnode: nodes, e.g. 'cern.cmsfronter1', ... or 'cern.Sum123'
#  histday_plot: how many days to plot: 2 or 7?
               

  # these are dicts to generate labels or titles in the figure
  yldict = {'mth':"Max Threads in 5 min", 'avt':"average response time (msec)"}
  yldict_summary = {'mth':"Max Threads in 5 min", 'avt':"average response time (msec)"}

  tldict = {'mth':"Max Threads of ", 'avt':"average response time (msec) of "}
  fldict = {'mth':"maxth", 'avt':"avrtime"}

  mycolor = ['r', 'g', 'b', 'y', 'c', 'm', '#7fff00', '#00007f', '#ff007f', '#7fff00']

  figs=[]

  for ist in sorted(mxth):
   if not sum(mxth[ist]['mth']) == 0:  # do not plot all zero plot

    plt.clf()  # clear figure 

    # convert list to np.array, vx: x-axis, vy: y-axis, note: y-value may be N*2 array
    vx = np.array(mxth[ist]['tms'])
    vy = np.array(mxth[ist][vflag])

    # vx0, vx1: axis limit
 
    vx0 = max( (time.time()-histday_plot*24*3600,min(mxth[ist]['tms']),) );
    vx1 = max(mxth[ist]['tms']);
    
    if len(np.shape(vy))==2:

      # idxl: 0 for Client response time, 1 for DB query time
      for idxl in range(np.shape(vy)[1]):
        if idxl == 0:
           fig = plt.fill_between(vx[vx>vx0], 0, vy[vx>vx0,idxl],color='g')
           fig1 = plt.plot(vx[vx>vx0], vy[vx>vx0,idxl],color='g', label='Client Response time')
        else:
           fig2 = plt.plot(vx[vx>vx0], vy[vx>vx0,idxl],color='b', label='DB query time')
      plt.legend()

    else:
           idxl=0
           fig = plt.plot(vx[vx>vx0], vy[vx>vx0],color=mycolor[idxl])

#    figs.append(fig)

    Ax=plt.gca()
    Ax.set_position([0.1,0.2,0.8,0.7])
#    Ax.set_xlim([max( (time.time()-histday*24*3600,min(mxth[ist]['tms']),) ),max(mxth[ist]['tms'])])

# add some
#    plt.ylabel(yldict[vflag])

#    plt.xlabel('Time (CERN)')

    # generate title. note: svnode might be 'cern.cmsfrontier1' or 'cern.Sum123'
    if not 'Sum' in svnode:
       plt.title(ist+' at '+svnode.split('.')[1])
       Ax.set_ylabel(yldict[vflag],fontsize='14')
    else:
       plt.title(ist+', Summary of servers')
       Ax.set_ylabel(yldict_summary[vflag],fontsize='14')


    x_label=[]
    x_tick=[]

    # define tick seperation, interval
    if histday_plot<3: 
       tintv = 7200
    elif histday_plot<10:
       tintv = 3600*6
    else:
       tintv = 3600*24

    # correct the timezone offset. calculate the ticks
    tzone = time.timezone - time.daylight*3600
    t0=np.floor( (vx0 - tzone) / tintv ) * tintv + tzone
    t1=np.ceil( (vx1 - tzone) / tintv  ) * tintv + tzone

    x_tick = np.arange(t0,t1,tintv)
    
    # generate tick labels; if 00h00, put a date
    for tt in list(x_tick):
      timelist=time.localtime(tt)
      if timelist[3]<(tintv/3600) and timelist[4]==0:
         x_label.append(time.strftime("%b/%d-%H:%M",timelist))
      else: 
         x_label.append(time.strftime("%H:%M",timelist))


    plt.xticks(x_tick, tuple(x_label), rotation=90)

    # set xlim to [vx0, vx1], set ylim to [0, vy1], where vy1 is the autumatically calculated one
    Ax.set_xlim([vx0,vx1])
    vy1 = Ax.get_ylim()[1]
    Ax.set_ylim([0,vy1])

    # set lable font size
    for alabel in Ax.get_xticklabels():
       alabel.set_fontsize('14')
    for alabel in Ax.get_yticklabels():
       alabel.set_fontsize('14')

#    h_figs = [[figs[icc][0] for icc in range(len(figs))]]
#    lgdtext = [sitelist[ii].replace(';','\n')  for ii in range(len(sitelist))]
#    lgd=plt.legend(tuple(h_figs), tuple(lgdtext), shadow=False, loc='upper left', bbox_to_anchor=(1,1))
#    for at in lgd.get_texts():
#       at.set_fontsize('9')
  

    F=plt.gcf()
    # get dpi and figure size: 960px * 720px
    # get_dpi() always give unwanted value ... FIXME

#    dpi=F.get_dpi()
    mydpi = 100;
    F.set_size_inches( (960./mydpi,720./mydpi) )
    F.savefig(imgdir+fldict[vflag]+'.'+svnode+'.'+ist+'.d%02d'%histday_plot+'.PNG',format='PNG',dpi=(mydpi))


def genreport(mxth,svnode,histday_plot):

  reportstr='<html>\n <head><title>Frontier Servlet Maxthreads Monitoring</title></head>\n'  
  reportstr+='<body>\n' 
  reportstr+='<h1  align="center">Frontier Servlet Maxthreads Monitoring for %s</h1>\n'%svnode
  reportstr+='<table  border="0" cellpadding="0" cellspacing="0" width="960px">\n'

  for ist in sorted(mxth):
   if not sum(mxth[ist]['mth']) == 0:
     reportstr+='<tr>\n<td><img src="'+imgdir+'maxth.'+svnode+'.'+ist+'.d%02d'%histday_plot+'.PNG" width=480px height=360px></td>\n' 
     reportstr+='<td><img src="'+imgdir+'avrtime.'+svnode+'.'+ist+'.d%02d'%histday_plot+'.PNG" width=480px height=360px></td>\n</tr>\n' 

  reportstr+='</table>\n'
  reportstr+='</body>\n' 
  reportstr+='</html>\n \n'
  return reportstr

def genmenu(mxth,svnode):

  if 'Sum' in svnode:
     nodename=svnode.split('.')[0]+'.Summary'
  else:
     nodename=svnode.split('.')[1]
  mstr=''
  mstr+=indent[2]+'<tr height="1"><td colspan="2" bgcolor="blue"></td></tr>\n'
  mstr+=indent[2]+'<tr>\n'
  mstr+=indent[3]+'<td>%s</td>\n'%nodename
  mstr+=indent[3]+'<td align="left">\n'
  mstr+=indent[4]+'<table width=100%>\n'

  for ist in sorted(mxth):
   if not sum(mxth[ist]['mth']) == 0:

     tmp_mth = np.array(mxth[ist]['mth'])
     tmp_avt = np.array(mxth[ist]['avt'])

     pfile = pgsdir+svnode+'.'+ist+'.html'
     pstr='<!DOCTYPE html><html lang="en">\n'  
     pstr+=indent[0]+'<head><title></title></head>\n'  
     pstr+=indent[0]+'<body>\n' 
     pstr+=indent[1]+'<h1 align="left"> %s %s </h1>\n'%(nodename, ist)
     pstr+=indent[1]+'<h2 align="left"> Latest value (in last 5 minutes): </h2>\n'
     pstr+=indent[1]+'<p>update time: '+time.ctime(mxth[ist]['tms'][-1])+' (local time of the site)</p>\n'
     if not 'Sum' in svnode:
        pstr+=indent[1]+'<p>Maximum threads: '+str(mxth[ist]['mth'][-1])+';   (Alarm threshold: '+str(int(mxth[ist]['th_th'][-1]))+')</p>\n'
     else:
        pstr+=indent[1]+'<p>Maximum threads: '+str(mxth[ist]['mth'][-1])+';   </p>\n'

     pstr+=indent[1]+'<p>Average client response time: '+'{0:.3f}'.format(mxth[ist]['avt'][-1][0])+' msec; Average DB query time: '+'{0:.3f}'.format(mxth[ist]['avt'][-1][1])+' msec</p>\n'
     pstr+=indent[1]+'<hr>\n'
     pstr+=indent[1]+'<h2 align="left"> Last 48 hours </h2>\n'
     pstr+=indent[1]+'<p>Maximum threads: '+str(max(tmp_mth[-48*12:]))+';</p>\n'
     pstr+=indent[1]+'<p>Average client response time: '+'{0:.3f}'.format(sum((tmp_avt[-48*12:,0])/(48*12)))+' msec; Average DB query time: '+'{0:.3f}'.format(sum((tmp_avt[-48*12:,1])/(48*12)))+' msec</p>\n'
     pstr+=indent[1]+'<table  border="0" cellpadding="0" cellspacing="0" width="960px">\n'
     pstr+=indent[2]+'<tr>\n'
     pstr+=indent[3]+'<td><img src="'+imgdir.replace('./','../')+'maxth.'+svnode+'.'+ist+'.d'+str(histday).zfill(2)+'.PNG" width=480px height=360px></td>\n' 
     pstr+=indent[3]+'<td><img src="'+imgdir.replace('./','../')+'avrtime.'+svnode+'.'+ist+'.d'+str(histday).zfill(2)+'.PNG" width=480px height=360px></td>\n'
     pstr+=indent[2]+'</tr>\n' 
     pstr+=indent[1]+'</table>\n'
     pstr+=indent[1]+'<hr>\n'
     pstr+=indent[1]+'<h2 align="left"> Last Week </h2>\n'
     pstr+=indent[1]+'<p>Maximum threads: '+str(max(tmp_mth[-24*7*12:]))+';</p>\n'
     pstr+=indent[1]+'<p>Average client response time: '+'{0:.3f}'.format(sum((tmp_avt[-24*7*12:,0])/(24*7*12)))+' msec; Average DB query time: '+'{0:.3f}'.format(sum((tmp_avt[-24*7*12:,1])/(24*7*12)))+' msec</p>\n'
     pstr+=indent[1]+'<table  border="0" cellpadding="0" cellspacing="0" width="960px">\n'
     pstr+=indent[2]+'<tr>\n'
     pstr+=indent[3]+'<td><img src="'+imgdir.replace('./','../')+'maxth.'+svnode+'.'+ist+'.d07.PNG" width=480px height=360px></td>\n' 
     pstr+=indent[3]+'<td><img src="'+imgdir.replace('./','../')+'avrtime.'+svnode+'.'+ist+'.d07.PNG" width=480px height=360px></td>\n'
     pstr+=indent[2]+'</tr>\n' 
     pstr+=indent[1]+'</table>\n'
     pstr+=indent[0]+'</body>\n' 
     pstr+='</html>\n \n'
     
     fpg = open(pfile,'w')
     fpg.write(pstr)
     fpg.close()

     #mstr+='<tr>\n<td><a href="'+svnode+'.'+ist+'.html" target="frame_main">'+ist+'</td>\n</tr>\n'
     mstr+=indent[5]+'<tr>\n'
     mstr+=indent[6]+'<td><a href="'+svnode+'.'+ist+'.html" target="frame_main">'+ist+'</a></td>\n'
     mstr+=indent[5]+'</tr>\n'

  mstr+=indent[4]+'</table>\n'
  mstr+=indent[3]+'</td>\n'
  mstr+=indent[2]+'</tr>\n'
  return mstr

def genmail(mxth, svnode):

  if not 'Sum' in svnode:

    for instance in mxth.keys():
      if mxth[instance]['mth'][-1]> mxth[instance]['th_th'][-1]:
         mailfilename = almdir+svnode+'.'+instance
         if not os.path.isfile(mailfilename):
            mailstr=''
            #mailstr += mailhead_alm
            mailstr += mailmsg_alm
            mailstr = mailstr.replace('TO_REPLACE_NODE','%s'%svnode)
            mailstr = mailstr.replace('TO_REPLACE_NUMBER','%d'%mxth[instance]['mth'][-1])
            mailstr = mailstr.replace('TO_REPLACE_INSTANCE','%s'%instance)
            mailstr = mailstr.replace('TO_REPLACE_THRESHOLD','%d'%mxth[instance]['th_th'][-1])
            print("mailstr"+mailstr)
            subject = 'Threads of TO_REPLACE_INSTANCE on TO_REPLACE_NODE exceed threshold'
            subject = subject.replace('TO_REPLACE_INSTANCE','%s'%instance)
            subject = subject.replace('TO_REPLACE_NODE','%s'%svnode)

            #fobj = open(mailfilename,'w')
            #fobj.write(mailstr)
            #fobj.close()
            #os.system('/usr/sbin/sendmail %s < %s'%(mailaddr, mailfilename))
            sender = 'dbfrontier@mail.cern.ch'  # From
            receivers = mailaddr  # To
            # copies = ''  # CC
            msg = MIMEMultipart('alternative')  # Using “alternative” indicates that the email message contains multiple representations of the same content, but in different formats. For example, you might have the same email message in both plain text and HTML.
            msg['Subject'] = subject
            msg['To'] = receivers
            msg.attach(MIMEText(emailtext, 'plain'))
            try:
                smtpObj = smtplib.SMTP('localhost')
                # smtpObj.set_debuglevel(True)
                # smtpObj.sendmail(sender, [receivers] + [copies], msg.as_string())
                smtpObj.sendmail(sender, [receivers], msg.as_string())
                smtpObj.quit()
                print("\nSuccessfully sent email to: " + msg['To'])
                # print(" CC'ed to: "+msg['CC'])
                time.sleep(1)
            except:
                print("\nFailed to send email to: " + msg['To'])


      if mxth[instance]['mth'][-1]<( mxth[instance]['th_th'][-1]/3 ):
         mailfilename = almdir+svnode+'.'+instance
         mailfilename_tmp = almdir+svnode+'.'+instance+'.reduced'
         if os.path.isfile(mailfilename):
            mailstr=''
            #mailstr += mailhead_red
            mailstr += mailmsg_red
            mailstr = mailstr.replace('TO_REPLACE_NODE','%s'%svnode)
            mailstr = mailstr.replace('TO_REPLACE_NUMBER','%d'%mxth[instance]['mth'][-1])
            mailstr = mailstr.replace('TO_REPLACE_INSTANCE','%s'%instance)
            mailstr = mailstr.replace('TO_REPLACE_THRESHOLD_LOW','%d'%(mxth[instance]['th_th'][-1]/3))
            subject = 'Threads of TO_REPLACE_INSTANCE on TO_REPLACE_NODE reduced'
            subject = subject.replace('TO_REPLACE_INSTANCE','%s'%instance)
            subject = subject.replace('TO_REPLACE_NODE','%s'%svnode)

            #fobj = open(mailfilename_tmp,'w')
            #fobj.write(mailstr)
            #fobj.close()
            #os.system('/usr/sbin/sendmail %s < %s'%(mailaddr, mailfilename_tmp))
            #os.system('rm %s'%mailfilename)
            #os.system('rm %s'%mailfilename_tmp)
            sender = 'dbfrontier@mail.cern.ch'  # From
            receivers = mailaddr  # To
            # copies = ''  # CC
            msg = MIMEMultipart('alternative')  # Using “alternative” indicates that the email message contains multiple representations of the same content, but in different formats. For example, you might have the same email message in both plain text and HTML.
            msg['Subject'] = subject
            msg['To'] = receivers
            msg.attach(MIMEText(emailtext, 'plain'))
            try:
                smtpObj = smtplib.SMTP('localhost')
                # smtpObj.set_debuglevel(True)
                # smtpObj.sendmail(sender, [receivers] + [copies], msg.as_string())
                smtpObj.sendmail(sender, [receivers], msg.as_string())
                smtpObj.quit()
                print("\nSuccessfully sent email to: " + msg['To'])
                # print(" CC'ed to: "+msg['CC'])
                time.sleep(1)
            except:
                print("\nFailed to send email to: " + msg['To'])


  pass

if __name__ == "__main__":
 indent = []
 stringg = ''
 for r in range(0,20):
   stringg += '  '
   indent.append(stringg)

 getconfig()

 # menustr: webpage for the left frame
 menustr=''
 menustr+='<!DOCTYPE html><html lang="en">\n'  
 menustr+=indent[0]+'<head><title></title></head>\n'  
 menustr+=indent[0]+'<body>\n'
 menustr+=indent[1]+'<table border="0" width="100%">\n'

 for site in sorted(siteconfig):
   siteinit(site);
   mxth_daycmbd_nodes=[] 
   for svnode in nodes:
     mxth_daylist = []
     for ii in range(7+1):
       #print(srcdir+pref1+svnode+'/'+pref2+svnode+'.'+time.strftime('%Y-%m-%d',time.localtime(time.time()-24*3600*(7-ii))))
       mxth = getInfo( srcdir+pref1+svnode+'/'+pref2+svnode+'.'+\
                      time.strftime('%Y-%m-%d',time.localtime(time.time()-24*3600*(7-ii)))\
                    )
       mxth_daylist.append(mxth)
 
#   hists=["1","2","3","4","5","6","7","8","9","10","11","12"]
#   hists=range(1,histhour+1)

#   for histid in hists:
#       ahist=getInfo("%s.%d"%(resultfile,histid))
#       sitests.append(ahist)

     mxth_daycmbd = combine(mxth_daylist)
     mxth_daycmbd_nodes.append(mxth_daycmbd)
   mxth_daycmbd_nodescmbd = combine_nodes(mxth_daycmbd_nodes)
   mxth_daycmbd_nodes.append( mxth_daycmbd_nodescmbd )

   for ii in range(len(mxth_daycmbd_nodes)): 
     mxth_c = mxth_daycmbd_nodes[ii]
     if ii < len(nodes):
       svnode = nodes[ii]
     else:
       svnode = 'SumServ'
     genchart(mxth_c,'avt',site+'.'+svnode,histday)
     genchart(mxth_c,'mth',site+'.'+svnode,histday)
     genchart(mxth_c,'avt',site+'.'+svnode,7)
     genchart(mxth_c,'mth',site+'.'+svnode,7)

     menustr_1 = genmenu(mxth_c,site+'.'+svnode)
     menustr += menustr_1
     genmail(mxth_c,site+'.'+svnode)
   
   # report str printed into stdout is only for old webpage
     reportstr = genreport(mxth_c,site+'.'+svnode,histday)
     print(reportstr)
     reportstr = genreport(mxth_c,site+'.'+svnode,7)
     print(reportstr)



 menustr+=indent[1]+'</table>\n'
 menustr+=indent[0]+'</body>\n'
 menustr+='</html>\n\n'

 fmenu=open(pgsdir+'/menu.html','w')
 fmenu.write(menustr)
 fmenu.close() 

 rc=0
 sys.exit(rc)
