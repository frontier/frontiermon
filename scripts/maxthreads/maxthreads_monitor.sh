#!/bin/bash

set -u  # Exit on any undefined variable
set -e  # Exit on any non-zero return code

here="`dirname $0`"

if [ $# = 0 ]; then 
    set -- atlas cms juno
fi

main ()
{
    vo_name=$1; shift

    imgdir="maxthreads_monitor.${vo_name}.plots"
    pgsdir="maxthreads_monitor.${vo_name}.pages"
    maildir="maxthreads_monitor.${vo_name}.mails"

    gen_base_dir="/home/dbfrontier/etc/maxthreads"
    web_base_dir="/home/dbfrontier/www/maxthreads"
    gen_image_dir="$gen_base_dir/$imgdir"
    gen_pages_dir="$gen_base_dir/$pgsdir"
    gen_mails_dir="$gen_base_dir/$maildir"

    echo "===================================="
    date
    echo "Generating MaxThreads stats for VO: ${vo_name}"
    echo

    rm -rvf $gen_image_dir; mkdir -vp $gen_image_dir
    rm -rvf $gen_pages_dir; mkdir -vp $gen_pages_dir
    mkdir -vp $gen_mails_dir

    webfiletmp="frontier_inst_maxthreads_old.${vo_name}.html"
    webfiletmp_path="$web_base_dir/$webfiletmp"
    python3 $here/maxthreads_monitor.py ${vo_name} > $webfiletmp_path

    webfile="frontier_inst_maxthreads.${vo_name}.html"
    webfile_path="$web_base_dir/$webfile"
    cat > $webfile_path <<EOF
<HTML>
<HEAD>
<TITLE>Frontier Servlet Maxthreads Monitoring</TITLE>
</HEAD>
<FRAMESET cols="20%, 75%">
  <FRAME name="frame_menu" src="$pgsdir/menu.html">
  <FRAME name="frame_main" src="$pgsdir/`ls $gen_pages_dir|head -1`">
  <NOFRAMES>
   Noframe??
  </NOFRAMES>
</FRAMESET>
</HTML>
EOF
    cp -vr $gen_image_dir $web_base_dir 
    cp -vr $gen_pages_dir $web_base_dir

    echo
    echo "MaxThreads stats generation complete."
}

for vo; do
    main $vo
done
