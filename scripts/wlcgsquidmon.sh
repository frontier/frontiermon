#!/bin/bash

SHELL=/bin/sh

sleep 5

cd /home/dbfrontier/etc/

wget -o timestamp.log -O timestamp1.txt http://wlcgsquidmon1.cern.ch/timestamp.txt
wget -a timestamp.log -O timestamp2.txt http://wlcgsquidmon2.cern.ch/timestamp.txt

if [[ ! (-s timestamp1.txt) ]]; then
   echo 0 > timestamp1.txt
fi
if [[ ! (-s timestamp2.txt) ]]; then
    echo 10000 > timestamp2.txt
fi

TIME1=$(cat timestamp1.txt)
TIME2=$(cat timestamp2.txt)
DIFF1=$(($TIME1-$TIME2))
DIFF2=$(($TIME2-$TIME1))

#echo $TIME1
#echo $TIME2
#echo $DIFF1
#echo $DIFF2

if [[ $DIFF1 -gt 3600 || $DIFF2 -gt 3600 ]] ; then
 echo "timestamp problem in wlcgsquidmon"
 if [ -r wlcgsquidmon ] 
  then
  echo " " > blank
  find ./wlcgsquidmon -mtime 2 -type f -delete
  else
  echo "timestamp problem in wlcgsquidmon" > wlcgsquidmon
  date >> wlcgsquidmon
  echo $TIME1 >> wlcgsquidmon
  echo $TIME2 >> wlcgsquidmon
  echo $DIFF1 >> wlcgsquidmon
  echo $DIFF2 >> wlcgsquidmon
  cat timestamp.log >> wlcgsquidmon
  mail -s "timestamp problem in wlcgsquidmon" bjb@jhu.edu < wlcgsquidmon
  mail -s "timestamp problem in wlcgsquidmon" wlcg-squidmon-support@cern.ch < wlcgsquidmon
 fi
else
rm -f wlcgsquidmon
fi  

rm -f timestamp1.txt
rm -f timestamp2.txt
rm -f timestamp.log   
