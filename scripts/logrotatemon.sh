#!/bin/bash

/usr/sbin/logrotate -s /home/dbfrontier/logs/logrotate.status /home/dbfrontier/conf/logrotate.conf

find /home/dbfrontier/data/awstats -mtime +365 ! -type d | xargs rm -f
find /home/dbfrontier/data/awstats -depth -type d | xargs rmdir 2> /dev/null
