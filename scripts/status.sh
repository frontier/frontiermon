#!/bin/bash

SHELL=/bin/sh


cd /home/dbfrontier/www/squidstats/images

cp greenball.gif ball.gif

if grep -q -E "warning|danger" ../squid_status.html
   then
    cp yellowball.gif ball.gif 
fi

if grep -q -E ">.*cern.*</a>" ../squid_status.html
   then
    cp redball.gif ball.gif
fi

if grep -q danger ../tomcat_status.html
   then
    cp redball.gif ball.gif
fi
