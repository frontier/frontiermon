#!/bin/bash

SHELL=/bin/sh

cd /home/dbfrontier/etc

wget -o timestampf.log -O timestamp1.txt http://frontier1.cern.ch/timestamp.txt
wget -a timestampf.log -O timestamp2.txt http://frontier2.cern.ch/timestamp.txt

if [[ ! (-s timestamp1.txt) ]]; then
   echo 0 > timestamp1.txt
fi
if [[ ! (-s timestamp2.txt) ]]; then
    echo 10000 > timestamp2.txt
fi

TIME1=$(cat timestamp1.txt)
TIME2=$(cat timestamp2.txt)
DIFF1=$(($TIME1-$TIME2))
DIFF2=$(($TIME2-$TIME1))

#echo $TIME1
#echo $TIME2
#echo $DIFF1
#echo $DIFF2

if [[ $DIFF1 -gt 3600 || $DIFF2 -gt 3600 ]] ; then
 echo "timestamp problem in frontiermon"
 if [ -r frontiermon ] 
  then
  echo " " > blank
  find ./frontiermon -mtime 2 -type f -delete
  else
  echo "timestamp problem in frontiermon" > frontiermon
  date >> frontiermon
  echo $TIME1 >> frontiermon
  echo $TIME2 >> frontiermon
  echo $DIFF1 >> frontiermon
  echo $DIFF2 >> frontiermon
  cat timestampf.log
  mail -s "timestamp problem in frontiermon" bjb@jhu.edu < frontiermon
  mail -s "timestamp problem in frontiermon" cms-frontier-alarm@cern.ch < frontiermon
 fi
else
rm -f frontiermon
fi  

rm -f timestamp1.txt
rm -f timestamp2.txt
rm -f timestampf.log
