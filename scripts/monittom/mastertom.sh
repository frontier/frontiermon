#!/bin/bash

SHELL=/bin/sh

cd /home/dbfrontier/scripts/monittom

echo "$(date '+%Y-%m-%d %H:%M:%S') - [INFO] Starting mastertom script"
./monittom.sh
./monitsig.sh

/home/dbfrontier/scripts/status.sh
