#!/bin/bash

let maxsecs="30 * 60"
emailaddr="cms-frontier-alarm@cern.ch dwd@cern.ch"

# $1 - status
# $2 - tomcat name
# $3 - html body
status () {
    typeset status=$1
    tomcat=${2/./}
    status_file=${tomcat}.status
    if [ ! -e $status_file ]; then
        echo "OK" > $status_file
    fi
    if ! grep -q $status $status_file; then
        if [ "$status" = "FAILED" ]; then
            text="$tomcat tomcat response problem"
            level="ERROR"
	elif [ "$status" = "OLD" ]; then
	    text="$tomcat tomcat database more than $maxsecs seconds old"
            level="WARNING"
        else
            text="$tomcat tomcat responding and up to date"
            level="INFO"
        fi
        echo $status > $status_file
        echo -e "$text\n"`date "+%Y-%m-%d %H:%M:%S"` | mail -s "$text" $emailaddr
        echo "$(date '+%Y-%m-%d %H:%M:%S') - [$level] $text"
    fi
    if [ "$status" != "OK" ]; then
        html_body="$3"
        read -r -d '' html_body << EOM
$html_body
          <tr>
            <td scope="row"></td>
            <td>
              <a target="_blank" href="http://frontier.cern.ch/maxthreads/maxthreads_monitor.cms.pages/cern.${2}.html">$tomcat</a>
            </td>
            <td>`date -r $status_file "+%Y-%m-%d %H:%M:%S"`</td>
            <td class="text-center" title="The last response is $status"><span class="badge badge-danger">$status</span></td>
          </tr>
EOM
    fi

    echo "$html_body"
}

home="/home/dbfrontier/"
cd ${home}etc/monittom

database_names=(FrontierArc FrontierPrep FrontierProd LumiProd PromptProd)
html_body=""
output="output.txt"
trap "rm -f $output" 0
for i in $(seq 1 4); do
    for j in ${database_names[@]}; do
	sql="select 1 from dual"
	if [ "$j" = "FrontierProd" ]; then
	    sql="select /*+ monitor */ last from cms_conditions.replication_test where key='TEST_0'"
	fi
        ${home}scripts/monittom/fnget.py --url=http://cmsfrontier${i}.cern.ch:8880/${j}/Frontier --sql="$sql" > $output
	status=FAILED
	if [ "$j" = "FrontierProd" ]; then
	    if grep -q "  LAST     TIMESTAMP" $output; then
		let age="$(date +%s) - $(TZ=UTC date --date="$(tail -1 $output|sed 's/^[ ]*.//')" +%s)"
		if [ -n "$age" ]; then
		    if [ $age -le $maxsecs ]; then
			status=OK
		    else
			status=OLD
		    fi
		fi
	    fi
	elif grep -q "  1     NUMBER" $output; then
	    status=OK
	fi
	html_body=$(status $status "cmsfrontier${i}.${j}" "$html_body")
    done
done

if [ -z "$html_body" ]; then
    html_div='\n      <div class="text-center">All tomcats are responding <span class="badge badge-success">OK</span></div>'
fi
read -r -d '' html << EOM
<html>
  <head>
    <title>Status of tomcats</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
      tbody>tr:hover {background: #cecece !important;}
      table {
        counter-reset: row-num;
      }
      table tr td:first-child::before {
        counter-increment: row-num;
        content: counter(row-num)".";
      }
    </style>
  </head>

  <body>
    <div class="container">
      <h1>List of tomcats currently experiencing problems</h1>
      <p>Last update was on `date "+%Y-%m-%d %H:%M:%S"`</p>
      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Tomcat</th>
            <th scope="col">Problem since</th>
            <th class="text-center" scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          $html_body
        </tbody>
      </table>$html_div
    </div>
  </body>
</html>
EOM

output_html="${home}www/squidstats/tomcat_status.html"
output_html_new="${output_html}.new"
output_html_old="${output_html}.old"
echo -e "$html" >> $output_html_new
if [ -f $output_html_old ]; then
    rm $output_html_old
fi
if [ -f $output_html ]; then
    ln $output_html $output_html_old
fi
mv $output_html_new $output_html

