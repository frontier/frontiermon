#!/bin/sh

shopt -s expand_aliases

DIR=/home/dbfrontier
SCRATCH=$DIR/etc/monitsig

source /cvmfs/cms.cern.ch/cmsset_default.sh

if [ ! -d "$SCRATCH" ]; then
    mkdir $SCRATCH
fi

cd $SCRATCH

CMSREL="`scram l|grep -B 1 "cms.cern.ch"|grep " CMSSW"|tail -1|awk '{print $2}'`"


if [ ! -d "$CMSREL" ]; then
    rm -rf CMSSW*
    scram p CMSSW $CMSREL
fi

cd $CMSREL/src
cmsenv

# $1 - status
# $2 - number of cmsfrontier machine
# $3 - output file
status () {
    status_file=../../cmsfrontier${2}Prodtomcat
    if [ ! -e $status_file ]; then
        echo "OK" > $status_file
    fi
    if ! grep -q $1 $status_file; then
        if [ "$1" = "FAILED" ]; then
            subject="Possible cmsfrontier${2}Prod tomcat signature problem"
        else
            subject="cmsfrontier${2}Prod tomcat signature has returned"
        fi
        echo $1 > $status_file
        send_mail "$subject" $3
    fi
}

# $1 - subject of email
# $2 - output file
send_mail () {
    attachment=cmsfrontierProdtomcat
    echo $1 > $attachment
    date >> $attachment
    cat $2 >> $attachment
    mail -s "$1" cms-frontier-alarm@cern.ch < $attachment
    rm -f $attachment
}

# $1 - number of cmsfrontier machine
# $2 - load balance alias
# $3 - output file
check_load_balance_alias () {
    if curl -s -H "X-Frontier-id: monitsig" "http://cmsfrontier"$1".cern.ch:8000/FrontierProd/Frontier/type=cert_request:1&encoding=pem" | openssl x509 -noout -text | grep -q DNS:$2; then
        status "OK" $1 $3
    else
        status "FAILED" $1 $3
    fi
}

output=output.txt
for i in $(seq 1 7); do
    echo 'select 1 from dual' | FRONTIER_SERVER="(serverurl=http://cmsfrontier"$i".cern.ch:8000/PromptProd)(security=sig)" FRONTIER_LOG_LEVEL=debug fn-req > $output
    if grep -q " 1 NUMBER" $output; then
        if [ "$i" -eq "5" ] || [ "$i" -eq "6" ]; then
            check_load_balance_alias $i "cmsfrontierdev.cern.ch" $output
        else
            check_load_balance_alias $i "cmsfrontier.cern.ch" $output
        fi
    else
        status "FAILED" $i $output
    fi
done

rm -f $output
